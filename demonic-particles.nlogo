globals [ color-range ]
turtles-own [ speed mass last-collision neighbor-speeds ]

to setup

  clear-all

  draw-walls

  create-turtles population [

    setxy random-xcor random-ycor
    avoid-walls

    set shape "circle"
    set size 0.5
    set mass 20

    ;; make sure the speed is never 0
    set speed random-float 1
    if speed = 0 [ set speed 0.1 ]

    ;; set the particle's color according to its speed. Red is fast, Sky-blue is
    ;; slow and white-ish is average
    ifelse (speed >= 0.5)
    [ set color scale-color red speed 1.5 0.5 ]
    [ set color scale-color sky speed -0.5 0.5 ]

    ;; keeps a record of the speeds of the other turtles in
    ;; its room
    set neighbor-speeds ( list speed )
  ]

  reset-ticks

end

to draw-walls

  ;; draw membrane
  ask patches with [ pxcor = 0 ] [
    set pcolor yellow
  ]

  ;; draw the surrounding walls
  ask patches with [
      pxcor = max-pxcor or
      pxcor = min-pxcor or
      pycor = max-pycor or
      pycor = min-pycor ] [ set pcolor brown ]

end

;; reposition turtles that are in a wall patch
to avoid-walls

  if pxcor = 0 [
      set xcor (xcor + one-of [1 -1])
    ]
    if pxcor = max-pxcor [
      set xcor xcor - 2
    ]
    if pxcor = min-pxcor [
      set xcor xcor + 2
    ]
    if pycor = max-pycor [
      set ycor ycor - 2
    ]
    if pycor = min-pycor [
      set ycor ycor + 2
    ]

end

to go

  ask turtles [
    check-wall-collision
    update-neighbor-speeds
    if collide? [ check-for-particlecollision ]

    let max-speed max [ speed ] of turtles

    ifelse (speed >= 0.5)
    [ set color scale-color red ( speed / max-speed ) 1.5 0.5 ]
    [ set color scale-color sky ( speed / max-speed ) -0.5 0.5 ]

    ; normalize the speed
    forward speed / max-speed
  ]
  tick

end

;; bounce when collides with any wall
to check-wall-collision

  ; check collision with middle wall
  if [ pxcor ] of patch-ahead 1 = 0 [
    ifelse [ pcolor ] of patch-ahead 1 = yellow
      [ decide-traspase-door ]
      [ set heading (- heading) ]
  ]

  ; check collision with borders
  if abs ( [ pxcor ] of patch-ahead 1 ) = max-pxcor [
    set heading (- heading)
  ]
  if abs [ pycor ] of patch-ahead 1 = max-pycor [
    set heading (180 - heading)
  ]

end

to decide-traspase-door

  ;; Calculate the difference between a particle speed and its log of other particles' speed
  let speed-dif abs ( mean neighbor-speeds - speed )

  ifelse speed-dif < traspasing-threshold
    [ set heading (- heading) ]
    [
      jump 2
      set neighbor-speeds ( list speed )
    ]

end

;; update the mean speed that each particle perceives according to
;; their encounters with other turtles
to update-neighbor-speeds

  set neighbor-speeds sentence neighbor-speeds [ speed ] of turtles-on neighbors

  if length neighbor-speeds > memory [
    set neighbor-speeds sublist neighbor-speeds memory length neighbor-speeds
  ]

end


;; From now on, collision is implemented. This code comes from the Connected Chemistry 1
;; (Bike Tire) model, which can be found in the models library. Copyright to Uri Wilensky,
;; licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.

to check-for-particlecollision ;; particle procedure
  if count other turtles-here  >= 1
  [
    ;; the following conditions are imposed on collision candidates:
    ;;   1. they must have a lower who number than my own, because collision
    ;;      code is asymmetrical: it must always happen from the point of view
    ;;      of just one particle.
    ;;   2. they must not be the same particle that we last collided with on
    ;;      this patch, so that we have a chance to leave the patch after we've
    ;;      collided with someone.
    let candidate one-of other turtles-here with
      [who < [who] of myself and myself != last-collision]
    ;; we also only collide if one of us has non-zero speed. It's useless
    ;; (and incorrect, actually) for two turtles with zero speed to collide.
    if (candidate != nobody) and (speed > 0 or [speed] of candidate > 0)
    [
      collide-with candidate
      set last-collision candidate
      ask candidate [ set last-collision myself ]
    ]
  ]
end

;; implements a collision with another particle.
;;
;; THIS IS THE HEART OF THE PARTICLE SIMULATION, AND YOU ARE STRONGLY ADVISED
;; NOT TO CHANGE IT UNLESS YOU REALLY UNDERSTAND WHAT YOU'RE DOING!
;;
;; The two turtles colliding are self and other-particle, and while the
;; collision is performed from the point of view of self, both turtles are
;; modified to reflect its effects. This is somewhat complicated, so I'll
;; give a general outline here:
;;   1. Do initial setup, and determine the heading between particle centers
;;      (call it theta).
;;   2. Convert the representation of the velocity of each particle from
;;      speed/heading to a theta-based vector whose first component is the
;;      particle's speed along theta, and whose second component is the speed
;;      perpendicular to theta.
;;   3. Modify the velocity vectors to reflect the effects of the collision.
;;      This involves:
;;        a. computing the velocity of the center of mass of the whole system
;;           along direction theta
;;        b. updating the along-theta components of the two velocity vectors.
;;   4. Convert from the theta-based vector representation of velocity back to
;;      the usual speed/heading representation for each particle.
;;   5. Perform final cleanup and update derived quantities.
to collide-with [ other-particle ] ;; particle procedure
  let mass2 0
  let speed2 0
  let heading2 0
  let theta 0
  let v1t 0
  let v1l 0
  let v2t 0
  let v2l 0
  let vcm 0



  ;;; PHASE 1: initial setup

  ;; for convenience, grab some quantities from other-particle
  set mass2 [mass] of other-particle
  set speed2 [speed] of other-particle
  set heading2 [heading] of other-particle

  ;; since turtles are modeled as zero-size points, theta isn't meaningfully
  ;; defined. we can assign it randomly without affecting the model's outcome.
  set theta (random-float 360)


  ;;; PHASE 2: convert velocities to theta-based vector representation

  ;; now convert my velocity from speed/heading representation to components
  ;; along theta and perpendicular to theta
  set v1t (speed * cos (theta - heading))
  set v1l (speed * sin (theta - heading))

  ;; do the same for other-particle
  set v2t (speed2 * cos (theta - heading2))
  set v2l (speed2 * sin (theta - heading2))


  ;;; PHASE 3: manipulate vectors to implement collision

  ;; compute the velocity of the system's center of mass along theta
  set vcm (((mass * v1t) + (mass2 * v2t)) / (mass + mass2) )

  ;; now compute the new velocity for each particle along direction theta.
  ;; velocity perpendicular to theta is unaffected by a collision along theta,
  ;; so the next two lines actually implement the collision itself, in the
  ;; sense that the effects of the collision are exactly the following changes
  ;; in particle velocity.
  set v1t (2 * vcm - v1t)
  set v2t (2 * vcm - v2t)


  ;;; PHASE 4: convert back to normal speed/heading

  ;; now convert my velocity vector into my new speed and heading
  set speed sqrt ((v1t * v1t) + (v1l * v1l))
  ;; if the magnitude of the velocity vector is 0, atan is undefined. but
  ;; speed will be 0, so heading is irrelevant anyway. therefore, in that
  ;; case we'll just leave it unmodified.
  if v1l != 0 or v1t != 0
    [ set heading (theta - (atan v1l v1t)) ]

  ;; and do the same for other-particle
  ask other-particle [
    set speed sqrt ((v2t ^ 2) + (v2l ^ 2))
    if v2l != 0 or v2t != 0
      [ set heading (theta - (atan v2l v2t)) ]
  ]

end
@#$#@#$#@
GRAPHICS-WINDOW
240
25
858
444
-1
-1
10.0
1
10
1
1
1
0
1
1
1
-30
30
-20
20
1
1
1
ticks
30.0

BUTTON
20
120
93
153
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
100
120
163
153
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
20
40
215
73
population
population
0
1000
100.0
1
1
NIL
HORIZONTAL

SLIDER
20
160
215
193
traspasing-threshold
traspasing-threshold
0
1
0.34
0.01
1
NIL
HORIZONTAL

MONITOR
20
265
145
310
mean speed left
mean [ speed ] of turtles with [ xcor < 0 ]
5
1
11

MONITOR
20
320
147
365
mean speed right
mean [ speed ] of turtles with [ xcor > 0 ]
5
1
11

SWITCH
20
200
127
233
collide?
collide?
1
1
-1000

PLOT
900
25
1250
220
Average speeds
ticks
mean-speed
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"left-side" 1.0 0 -8630108 true "" "plot mean [ speed ] of turtles with [ xcor < 0 ]"
"right-side" 1.0 0 -5825686 true "" "plot mean [ speed ] of turtles with [ xcor > 0 ]"

SLIDER
20
80
215
113
memory
memory
0
100
19.0
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

The Maxwell’s demon is a thought experiment in which there are two rooms, A and B, filled with gas (particles in movement) and connected by a membrane. A creature (called the demon) opens and closes that membrane so that only the faster particles go from A to B, and only  the slower particles  go from B to A. This contradicts the second law of thermodynamics because at the end the particles would be separated according to their  velocities, reverting the entropy.

In this model, a more distributed approach is used. Instead of a single demon opening and closing a membrane, the particles (which are the agents) themselves will decide wether to cross it according to the local information they've gathered.

## HOW IT WORKS

At setup time, particles are assigned a random position and speed. The faster ones are coloured with red, while the slower ones are coloured with blue. The white particles have a speed near the average. When two particles pass close to each other (in neighboring patches), they log the other's speed.

Collisions between two particles affect the speed and direction of both particles, and the calculations depend on their original speed, direction and mass. In this model, the mass is assumed to be constant (20) across all the particles.

The environment consists of two rooms separated by a yellow membrane in the middle. The brown walls are solid. That is, the particles bounce when they collide with them. On the other hand, when the particles are about to cross the membrane, they evaluate whether their speed is too different from the mean speed of the neighboring particles they've encountered. If they find this true they cross it, else they bounce. The loss of speed caused by the collision between a particle and a wall or the membrane is ignored in this model. A particle's memory (log of neighbors' speeds) is cleared when it pass through the membrane.

## HOW TO USE IT

**1.** Vary the parameters to your like (population will only take effect at SETUP time).

**2.** Press setup to initialize the model. Two rooms should appear, filled with a bunch of particles at random positions.

**3.** Press go to run the model. Particles will start moving and the monitors and plots will vary.

**4.** You can vary the parameters at run time (except for population) and see how they affect the model. Make only one change at a time in order to observe the responses.

### Inputs / Parameters

**population:** The number of particles in the whole model.

**memory:** This controls the length of the *neighbor-speeds* property, in which each particle keeps a record of the speed of the other particles that has passed by near.

**traspasing-threshold:** If the difference between a particle's speed and the mean of the neighbors' speed is greater than this threshold, the particle will cross through the membrane.

**collide:** Whether to treat the particles as solid objects and calculate collisions. This code comes from the Connected Chemistry 1 (Bike Tire) model, which can be found in the models library.

### Outputs

The monitors and the plot show the variation of the average speed on each room. In the Maxwell's demon thought experiment, the difference of those averages between rooms should increase as time passes, until reaching certain stability.

## THINGS TO NOTICE

* With what configuration do the particles organize themselves according to their speed more easily? How does the speed of the particles change when they collide?

* What does it mean that a particle has a high ```speed``` in the model? How does it affect the speed of the actual particle in the display? Does the colour of the particles change dynamically according to their speed?

* Do the particles always end up separated according to their speed? Is this easy to achieve in the model or does it need a special configuration of the parameters? When separation occurs, do the rooms always end up with the same kind of particles (slower or faster)?

* When the system is constantly changing, does the difference of the average speed between the rooms stabilize or oscillate?

## THINGS TO TRY

* Let the model run with the default parameters for a while (adjusting the speed of the model to faster might help). Watch the plot and the monitor of the mean speed for each room. How do the speeds vary for each room? Adjust the speed of the model back to normal. How does the display look now in respect to the begining?

* Turn the ```collide?``` switch. Observe the direction and speed of the particles. How does this affect the dynamism of the system as a whole?

* Play with the ```traspasing-threshold``` slider. Do the same value have different effects between two runs of the model? If not, why do this happen?

* Play with the ```memory``` slider. A small value means that each particle will only have into account the last encounters when deciding to cross the membrane.

## EXTENDING THE MODEL

* Let the user choose the membrane's size. A smaller membrane will lead to a smaller probability of a particle hiting it, so the model will be more static and the particles will have a more updated view of the mean speed. Would this affect the distribution of the particles?

* Express the temperature of each room according to their average speeds. This would be a pretty nice extra, that would make it easier to show the model to people unexperienced in the laws of thermodynamics.

## NETLOGO FEATURES

This model features a more agent-based approach to an old thought experiment, in the sense that the calculations about the average speed on each room are made by each particle and based on local information. Netlogo's views of the agents (turtles) allowed to manage the operations of thousands of them in an easy way.

## RELATED MODELS

* See Connected Chemistry folder from the Netlogo models library.

* See GasLab folder from the Netlogo models library.

* There's an alternative implementation of the Maxwell's demon thought experiment in the GasLab folder.

## CREDITS AND REFERENCES

Copyright 2016 *Paula A. Palacios*, *Sebastian Narvaez R.* and 2004 *Uri Wilensky*\*.

![CC BY-NC-SA 3.0](http://ccl.northwestern.edu/images/creativecommons/byncsa.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

\* This model contains part of the code from the Connected Chemistry 1 (Bike Tire) model, by Uri Wilensky, which is also licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
